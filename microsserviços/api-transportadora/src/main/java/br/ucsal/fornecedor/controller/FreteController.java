package br.ucsal.fornecedor.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ucsal.fornecedor.model.Frete;
import br.ucsal.fornecedor.service.FreteService;

@RestController
@RequestMapping("api/transportadora")
public class FreteController {

	@Autowired
	private FreteService serv;
	
	@PostMapping("/solicitarTransporte")
	public Frete solicitarTransporte(@RequestParam("itens") List<Long> itens,
									 @RequestParam("quantidade") List<Integer> quantidade) throws IOException, InterruptedException {	
		return serv.novoFrete(itens,quantidade);
	}
	
}