package br.ucsal.fornecedor.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ucsal.fornecedor.client.FeignClient;
import br.ucsal.fornecedor.model.Frete;
import br.ucsal.fornecedor.repository.FreteRepository;

@Service
public class FreteService {
	
	@Autowired
	private FeignClient cli;
	
	@Autowired
	private FreteRepository repo;
	
	public Frete novoFrete(List<Long> itens, List<Integer> quantidade) throws IOException, InterruptedException {
		Map<Long,Integer> itensQuantidade = new HashMap<Long,Integer>();
		
		for (int i = 0; i < itens.size(); i++) {
			itensQuantidade.put(itens.get(i), quantidade.get(i));
		}
		
		Frete frete = new Frete(itensQuantidade,(int)calcularNumVeiculos(itensQuantidade));
		
		frete = this.repo.save(frete);
		
		cli.realizarEntrega(frete.getItens());
		return frete;
	}

	private double calcularNumVeiculos(Map<Long,Integer> itensQuantidade) {
		double TotalNumItens = 0;
		for (Map.Entry<Long, Integer> entry : itensQuantidade.entrySet()) {
			Integer val = entry.getValue();
			TotalNumItens += val;
		}
		return Math.ceil(TotalNumItens/50);
	}
}