package br.ucsal.fornecedor.client;

import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@org.springframework.cloud.openfeign.FeignClient(name = "transportadora", url = "http://localhost:8082/api/armazem")
public interface FeignClient {

	@PostMapping("/receberEntrega")
	public void realizarEntrega(@RequestBody Map<Long,Integer> entrega);
	
}