package br.ucsal.fornecedor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.fornecedor.model.Frete;

@Repository
public interface FreteRepository extends JpaRepository<Frete, Long> {

}
