package br.ucsal.loja.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ucsal.loja.client.FeignClient;
import br.ucsal.loja.model.Venda;

@Service
public class VendaService {
	
	@Autowired
	private FeignClient cli;

	public Venda realizarVenda(Map<Long,Integer> venda) {
		cli.ChecarEstoque(venda);
		return null;
	}
	
}