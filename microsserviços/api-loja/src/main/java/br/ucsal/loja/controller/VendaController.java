package br.ucsal.loja.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ucsal.loja.model.Venda;
import br.ucsal.loja.service.VendaService;

@RestController
@RequestMapping("api/loja")
public class VendaController {
	
	@Autowired
	private VendaService serv;
	
	@PostMapping("/comprar")
	public Venda comprar(@RequestBody Map<Long,Integer> venda) {
		return serv.realizarVenda(venda);
	}
}