package br.ucsal.armazem.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ucsal.armazem.service.EstoqueService;

@RestController
@RequestMapping("api/armazem")
public class EstoqueController {

	@Autowired
	private EstoqueService serv;

	public EstoqueController(EstoqueService estoqueService) {
        this.estoqueService = estoqueService;
    }
	
	@PostMapping("/receberEntrega")
	public void receberEntrega(@RequestBody Map<Long,Integer> entrega) {
		serv.armazenar(entrega);

		if (estoqueService.estoqueInsuficiente(entrega)) {
            estoqueService.solicitarItensAoFornecedor();
	}
	
	}
}