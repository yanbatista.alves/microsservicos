package br.ucsal.armazem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiArmazemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiArmazemApplication.class, args);
	}

}
