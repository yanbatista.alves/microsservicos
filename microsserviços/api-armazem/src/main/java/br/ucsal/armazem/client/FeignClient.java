package br.ucsal.loja.client;

import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@org.springframework.cloud.openfeign.FeignClient(name = "estoque", url = "http://localhost:8082/api/armazem")
public interface FeignClient {

	@PostMapping("/ChecarEstoque")
	public void ChecarEstoque(@RequestBody Map<Long,Integer> compra);
	
}