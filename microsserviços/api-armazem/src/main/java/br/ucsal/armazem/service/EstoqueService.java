package br.ucsal.armazem.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import br.ucsal.armazem.model.Estoque;
import br.ucsal.armazem.repository.EstoqueRepository;

@Service
public class EstoqueService {

	
	@Autowired
	private EstoqueRepository repo;
	
	public void armazenar(Map<Long,Integer> entrega) {
		for (Map.Entry<Long, Integer> entry : entrega.entrySet()) {
			Long idProduto = Long.valueOf(entry.getKey());
			Estoque e = repo.findById(idProduto).get();
			
			Integer quantidade = (Integer)entry.getValue();
			e.setQuantidade(e.getQuantidade()+quantidade);
			
			this.repo.save(e);	
		}
	}

	 private static final int QUANTIDADE_DE_ITENS = 5;

	@Autowired
	 private FeignClientFornecedor feignClientFornecedor;

	 public void solicitarItensAoFornecedor() {
        feignClientFornecedor.enviarValorTotal(QUANTIDADE_DE_ITENS);
    }
}