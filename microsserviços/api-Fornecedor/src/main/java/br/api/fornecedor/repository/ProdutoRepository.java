package br.api.fornecedor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.api.fornecedor.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
