package br.api.fornecedor.repository;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@org.springframework.cloud.openfeign.FeignClient(name = "microservice", url = "http//localhost:8081/")
public interface FeignClientFornecedor {
	
	
	
	@PostMapping("/api/requisicao")
	String enviarValorTotal(@RequestBody int valorTotal);

}
