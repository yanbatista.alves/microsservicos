package br.api.fornecedor.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.api.fornecedor.model.Produto;
import br.api.fornecedor.repository.FeignClient;
import br.api.fornecedor.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
    private FeignClient feio; 
	
	@Autowired
	private ProdutoRepository repo;

	public int calcularValorTotal(Map<Long, Integer> prodQtd) {
		int valorTotal = 0;

		for (Map.Entry<Long, Integer> entry : prodQtd.entrySet()) {
			Long produtoId = entry.getKey();
			Integer quantidade = entry.getValue();

			Produto produto = repo.findById(produtoId).orElse(null);
			if (produto != null) {
				int valorProduto = produto.getValor();
				valorTotal += valorProduto * quantidade;
			}
		}
		
		

		return valorTotal;
	}
}
