package com.api.fornecedor.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.api.fornecedor.service.ProdutoService;

@RestController
@RequestMapping("api/fornecedor")
public class FornecedorController {

	private ProdutoService produtoService;

	@Autowired
	public FornecedorController(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}

	@GetMapping("/pedido")
	public Integer calcularValorTotalPedido(@RequestBody Map<Long, Integer> prodQtd) {
		int valorTotal = produtoService.calcularValorTotal(prodQtd);
		return valorTotal;
	}
	
}
